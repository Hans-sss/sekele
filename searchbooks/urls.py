from django.urls import path
from . import views

app_name = 'helloworld'

urlpatterns = [
    path('',views.page, name='index'),
    path('search', views.search, name='search'),
]