from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from course.models import Enrollment
from django.contrib.auth.decorators import login_required
import requests
import json

# Create your views here.
@login_required(login_url='/account/login/')
def page(request):
    User = request.user
    list_course = Enrollment.objects.filter(member=User)
    if (request.method == 'GET'):
        response = {
            'User' : User,
            'listCourse':list_course,
        }
        return render(request, 'searchbooks/search.html', response)

def search(request):
    query = request.GET['search_key']
    search_url = 'https://www.googleapis.com/books/v1/volumes?q=' + query +'&maxResults=12'
    response = requests.get(search_url)

    json_result = json.loads(response.content)
    return JsonResponse(json_result, safe = False)