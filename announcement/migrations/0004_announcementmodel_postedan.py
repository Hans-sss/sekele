# Generated by Django 3.0.5 on 2020-11-14 06:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('announcement', '0003_auto_20201114_1327'),
    ]

    operations = [
        migrations.AddField(
            model_name='announcementmodel',
            name='postedAn',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
