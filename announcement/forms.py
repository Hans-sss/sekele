from django import forms
from .models import AnnouncementModel

class AnnouncementForm(forms.ModelForm):
    class Meta:
        model = AnnouncementModel
        fields = [
            'judulAn',
            'isiAn',
        ]

        widgets = {
            'judulAn'   : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'ex. Pemberitahuan penting ...',
                    'id':'id_judulAn'
                }
            ),
            'isiAn'     : forms.Textarea(
                attrs={
                    'class':'form-control',
                    'placeholder':'ex. Halo semua, bla bla bla ...',
                    'id':'id_isiAn'
                }
            )
        }