from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.

class AccountAdmin(UserAdmin):
    list_display = ('nomorinduk', 'username', 'date_joined', 'date_of_birth', 'is_admin', 'is_staff')
    search_fields = ('nomorinduk', 'username')
    readonly_fields = ('id', 'date_joined')

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(Account, AccountAdmin)
