from django.db import models
from django.contrib.auth.models import(BaseUserManager, AbstractBaseUser, PermissionsMixin)

# Create your models here.
class MyAccountManager(BaseUserManager):
    def create_user(self, nomorinduk, username, date_of_birth, password=None):
        if not nomorinduk:
            raise ValueError("!")
        if not username:
            raise ValueError("!")
        user = self.model(
            nomorinduk = nomorinduk,
            username = username,
            date_of_birth = date_of_birth,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, nomorinduk, username, date_of_birth, password):
        user = self.create_user(
            nomorinduk = nomorinduk,
            username = username,
            password = password,
            date_of_birth = date_of_birth,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser= True
        user.save(using=self._db)
        return user

class Account(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, unique=True)
    nomorinduk = models.CharField(max_length=30, unique=True, verbose_name="Nomor Induk")
    date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
    date_of_birth = models.DateField(verbose_name='tanggal lahir')
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = MyAccountManager()

    USERNAME_FIELD = 'nomorinduk'
    REQUIRED_FIELDS = ['username', 'date_of_birth']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin   
    
    def has_module_perms(self, app_label):
        return True
